<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class IndexController extends Controller
{
    public function envConfig()
    {
        $environment = App::environment(); //获取APP_ENV
        dump($environment);

        $value = config('app.timezone'); //获取配置文件
        dump($value);

        config(['app.timezone' => 'America/Chicago']); //要在运行时设置配置值

        //php artisan config : cache 配置缓存

        //php artisan down  php artisan up 关闭开启维护模式
    }
}
